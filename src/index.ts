import express from "express";
import mongoose from "mongoose";
import bodyParser from "body-parser";
import cors from "cors";
import { authMiddleware  } from "../controllers/auth.controller";
import { authRouter } from "../routers/auth.router";
import { profileRouter } from "../routers/profile.router"
import { postRouter } from "../routers/post.router";
import { friendsRouter } from '../routers/friend.router'
import { findNewFriendsRouter } from '../routers/findNewFriends.router';
import { chatRouter } from "../routers/chat.router";
import { sendMessage } from "../controllers/chat.controller"
import { Server } from "socket.io"

const app = express();
const PORT = 3050;
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.json());
app.use(cors());

app.use("/auth", authRouter);
app.use("/profile", authMiddleware, profileRouter);
app.use("/posts", authMiddleware, postRouter);
app.use("/friends", authMiddleware, friendsRouter);
app.use("/findnewfriends", authMiddleware, findNewFriendsRouter)
app.use("/chats", authMiddleware, chatRouter);

const start = async () => {
  try {
    await mongoose.connect(
      "mongodb://localhost:27017/socialNetwork",
      {
        useNewUrlParser: true,
        useCreateIndex: true,
        useUnifiedTopology: true,
      },
      () => {
        console.log("connected to database");
      }
    );

    const server = app.listen(PORT, () =>  console.log(`The application is listening on port ${PORT}`))

    const io = new Server(server)
      io.on("connection", (socket) => {
          socket.on('sendMessage', async (message) => {
              const messages = await sendMessage(message)
              io.emit('tweet', messages);        
          })
      })
  } catch (err){
    console.log(err)
  }
}
//socket room


start();