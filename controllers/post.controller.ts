import { Post, IPost, postSchema } from "../models/post.model";
import { User, IUser } from "../models/user.model";
import { Request, Response} from 'express';

interface CustomRequest<T> extends Request {
    body: T
} 

const createPost = async function (request: CustomRequest<IPost>, response: Response){
    try{
        const { description, media} = request.body
        const newPost = await Post.create({ owner: request.user._id, description, media})       
        const postCreator = await User.findById({_id: request.user._id});
        if (postCreator){
            postCreator.posts.push(newPost._id)
            await postCreator.save()
            const posts = postCreator.posts
            return response.json({posts})
        }

    } catch (err) {
        console.log(err)
        return response.status(500).json({message: "Something went wrong. Cant create this post."})
    }
}

const deletePost = async function (request: CustomRequest<IPost>, response: Response) {
    try {
        console.log(request.body)
        let post = await Post.findOne({ _id: request.body._id });
        let user = await User.findOne({ _id: request.user._id });
        if (!post) {
            return response.status(500).json({message: "Can't find this post."})
        }
        if (!user) {
            return response.status(500).json({message: "Something went wrong."})
        } else {
            if ( post.owner._id.toString() !== request.user._id.toString()) {
                return response.status(400).json({message: "It's not your post and you don't have access to delete this post."})
            }
            Post.deleteOne({ _id: request.body._id });
            for (let i = 0; i < user.posts.length; i++) {
                if (user.posts[i]._id.toString() == post._id.toString()) {
                    user.posts.splice(i, 1);
                    await user.save();
                }
            }
            const posts = user.posts
            response.json({posts});
        }
    }
    catch (err) {
        response.status(500).json({message: "Something went wrong. Can't find this order."})
    }
}

const getMyPosts = async function (request: CustomRequest<IUser>, response: Response){
    try {
        const postsOwner = await User.findOne({_id: request.user._id})
        if (!postsOwner) {
            return response.status(500).json({message: "Something went wrong."})
        } else {
            const posts = postsOwner.posts
            return response.json({posts})
        }
    } catch (err){
        console.log(err)
        return response.status(500).json({message: "Something went wrong."})
    }
}
//============================================================================================Get all Posts

const getAllPosts = async function (request: CustomRequest<IUser>, response: Response){
    try {
        const posts = await Post.find({})
        if (!posts) {
            return response.status(500).json({message: "No posts"})
        } else {
            return response.json({posts})
        }
    } catch (err){
        console.log(err)
        return response.status(500).json({message: "Something went wrong. Try again"})
    }
}

//==============================================================================================Get my friends POSTS
const getMyFriendsPost = async function (request: CustomRequest<IUser>, response: Response){
    try {
        const friendsOwner = await User.findOne({_id: request.user._id})
        if (!friendsOwner) {
            return response.status(500).json({message: "Something went wrong."})          
        }
        const friendsArray = friendsOwner.friends
        let myFriendsPosts: Array<IPost> = []
        friendsArray.forEach((friend: any) => {
            friend.posts.forEach((post: any) => {
                myFriendsPosts.push(post)
            });
        })
        return response.json({myFriendsPosts})
    } catch (err) {
        console.log(err)
        return response.status(500).json({message: "Something went wrong."})
    }
}

const likePost = async function (request: CustomRequest<IUser>, response: Response){
    try {
        console.log(request.user._id)
        console.log(request.body._id)
        const post = await Post.findOne({_id: request.body._id});

        if (post) {
        console.log(post.likes)
        const isAlreadyLiked = post.likes.includes(request.user._id); //indexOf тоже не пашет =(
        console.log(isAlreadyLiked)
        if (!isAlreadyLiked) {
          await Post.updateOne({_id: request.body._id}, {likes: [...post.likes, request.user._id]})
        } else {
          await Post.updateOne({_id: request.body._id}, {$pull: {likes: request.user._id}})
        }
        const posts = await Post.find({})
        const friendsOwner = await User.findOne({_id: request.user._id}) 
        if (!friendsOwner) {
            return response.status(500).json({message: "Something went wrong."})          
        }
        const friendsArray = friendsOwner.friends
        let myFriendsPosts: Array<IPost> = []
        friendsArray.forEach((friend: any) => {
            friend.posts.forEach((post: any) => {
                myFriendsPosts.push(post)
            });
        })
        const myPosts = friendsOwner.posts
        return response.status(200).json({posts, myFriendsPosts, myPosts})
        } 
    } catch (err) {
        console.log(err)
        return response.status(500).json({message: "Something went wrong."})
    }
}

export { createPost, deletePost, getAllPosts, getMyFriendsPost, getMyPosts, likePost  }