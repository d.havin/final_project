import AuthForm from './components/LoginRegistration/Auth.component';
import RegistrationForm from './components/LoginRegistration/Registration.component';
import { BrowserRouter } from 'react-router-dom';
import PrivateRoute from './components/PrivateRoute.component';
import PublicRoute from './components/PublicRoute';
import loggedPage from './components/LoggedPage/loggedPage';

function App() {
  return (
    <BrowserRouter>
      <PublicRoute exact path ='/' component = {AuthForm}/>
      <PublicRoute exact path = '/registration' component = {RegistrationForm}/>
      <PrivateRoute exact path = '/profile' component = {loggedPage}/>
    </BrowserRouter>
  );
}

export default App;
