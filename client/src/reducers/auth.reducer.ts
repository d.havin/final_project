import { REQUEST_REGISTRATE_USER, REQUEST_REGISTRATE_USER_SUCCESS, REQUEST_REGISTRATE_USER_FAILED, 
    REQUEST_LOGIN_USER, REQUEST_LOGIN_USER_SUCCESS, REQUEST_LOGIN_USER_FAILED, 
    REQUEST_CHANGE_AVATAR, REQUEST_CHANGE_AVATAR_SUCCESS, REQUEST_CHANGE_AVATAR_FAILED, APPLY_INVITATION_SUCCESS
} from "../constants/constants";

const defaultState = {
    loading : false,
    error: false,
    message : "",

    logedUser : {},
    isAuth : false,
    token : "",
}

export default function authReducer (state = defaultState, action : any){
    switch (action.type) {

        case REQUEST_REGISTRATE_USER:
            return {
                ...state,
                loading : true,
            }
        case REQUEST_REGISTRATE_USER_SUCCESS:
            return {
                ...state,
                message: "Successfull. Go to login page",
                loading : false,
            }
        case REQUEST_REGISTRATE_USER_FAILED:
            console.log(action.data)
            return {
                ...state,
                message: action.data,
                error: true,
            }
        case REQUEST_LOGIN_USER:
            return {
                ...state,
                loading : true
            }
        case REQUEST_LOGIN_USER_SUCCESS:
            return {
                message: "Successfull",
                token: action.data.token,
                logedUser : action.data.user,
                loading: false,
                error: false,
                isAuth : true
            }; 

        case REQUEST_LOGIN_USER_FAILED:
            return {
                ...state,
                loading: false,
            }; 
        case REQUEST_CHANGE_AVATAR:
            return {
                ...state,
                loading : true,
            }
        case REQUEST_CHANGE_AVATAR_SUCCESS:
            return {
                ...state, 
                loading : false,
                logedUser: action.data.updatedUser
            }
        case REQUEST_CHANGE_AVATAR_FAILED:
            return {
                ...state,
                error: true
            }
        // case LOGOUT_USER:
        //     localStorage.removeItem("token")
        //     return {
        //         ...state,
        //         logedUser : {},
        //         isAuth : false,
        //     };

        default:
            return state;
    }
}
