import { 
    FETCH_REGISTRATE_USER, REQUEST_REGISTRATE_USER, REQUEST_REGISTRATE_USER_SUCCESS, REQUEST_REGISTRATE_USER_FAILED, 
    FETCH_LOGIN_USER, REQUEST_LOGIN_USER, REQUEST_LOGIN_USER_SUCCESS, REQUEST_LOGIN_USER_FAILED, 
    FETCH_CHANGE_AVATAR, REQUEST_CHANGE_AVATAR, REQUEST_CHANGE_AVATAR_SUCCESS, REQUEST_CHANGE_AVATAR_FAILED,

    FETCH_GET_ALL_MY_POSTS, GET_ALL_MY_POSTS_REQUEST, GET_ALL_MY_POSTS_REQUEST_SUCCESS, GET_ALL_MY_POSTS_REQUEST_FAILED, 
    FETCH_GET_ALL_POSTS, GET_ALL_POSTS_REQUEST, GET_ALL_POSTS_REQUEST_SUCCESS, GET_ALL_POSTS_REQUEST_FAILED,
    FETCH_GET_ALL_MY_FRIENDS_POSTS, GET_ALL_MY_FRIENDS_POSTS_REQUEST, GET_ALL_MY_FRIENDS_POSTS_REQUEST_SUCCESS, GET_ALL_MY_FRIENDS_POSTS_REQUEST_FAILED,

    FETCH_ADD_POST, REQUEST_ADD_POST, REQUEST_ADD_POST_SUCCESS, REQUEST_ADD_POST_FAILED, 
    FETCH_DELETE_POST, REQUEST_DELETE_POST, REQUEST_DELETE_POST_SUCCESS, REQUEST_DELETE_POST_FAILED, 
    
    FETCH_GET_ALL_USERS, GET_ALL_USERS_REQUEST, GET_ALL_USERS_REQUEST_SUCCESS, GET_ALL_USERS_REQUEST_FAILED,
    FETCH_DELETE_FRIEND, DELETE_FRIEND_SUCCESS, 
    
    FETCH_ADD_FRIEND, ADD_FRIEND_REQUEST_SUCCESS,
    FETCH_CANCEL_REQUEST, CANCEL_REQUEST_SUCCESS,
    FETCH_APPLY_INVITATION, APPLY_INVITATION_SUCCESS,
    FETCH_REJECT_INVITATION, REJECT_INVITATION_SUCCESS,

    FETCH_GET_ALL_MY_INVITATIONS, GET_ALL_MY_INVITATIONS_SUCCESS,
    FETCH_GET_ALL_MY_REQUESTS_SUCCESS, GET_ALL_MY_REQUESTS_SUCCESS,
    FETCH_GET_ALL_MY_FRIENDS, GET_ALL_MY_FRIENDS_SUCCESS,

    FETCH_LIKE_POST, LIKE_POST_SUCCESS,

    FETCH_CREATE_CHAT,
    CREATE_CHAT_SUCCESS,

    GET_CHAT_SUCCESS,
    FETCH_GET_CHAT
} from "../constants/constants";

export const userRegistrationRequest = () => ({type: REQUEST_REGISTRATE_USER});
export const userRegistrationRequestSuccess = (data: any) => ({type: REQUEST_REGISTRATE_USER_SUCCESS, data});
export const userRegistrationRequestFailed = () => ({type: REQUEST_REGISTRATE_USER_FAILED});
export const fetchRegistrationNewUser = (data: any) => {return({type: FETCH_REGISTRATE_USER, data})};

export const userLoginRequest = () => ({type: REQUEST_LOGIN_USER})
export const userLoginRequestSuccess = (data: any) => ({type: REQUEST_LOGIN_USER_SUCCESS, data})
export const userLoginRequestFailed = () => ({type: REQUEST_LOGIN_USER_FAILED})
export const fetchLoginUser = (data: any) => ({type: FETCH_LOGIN_USER, data});

export const changeAvatarRequest = () => ({type: REQUEST_CHANGE_AVATAR});
export const changeAvatarRequestSuccess = (data: any) => ({type: REQUEST_CHANGE_AVATAR_SUCCESS, data});
export const changeAvatarRequestFailed = () => ({type: REQUEST_CHANGE_AVATAR_FAILED});
export const fetchChangeAvatarRequest = (data: any) => ({type: FETCH_CHANGE_AVATAR, data});

export const getAllMyPostsRequest = () => ({type: GET_ALL_MY_POSTS_REQUEST});
export const getAllMyPostsRequestSuccess = (data: any) => ({type: GET_ALL_MY_POSTS_REQUEST_SUCCESS, data});
export const getAllMyPostsRequestFailed = () => ({type: GET_ALL_MY_POSTS_REQUEST_FAILED});
export const fetchGetAllMyPosts = () => ({type: FETCH_GET_ALL_MY_POSTS});

export const getAllPostsRequest = () => ({type: GET_ALL_POSTS_REQUEST});
export const getAllPostsRequestSuccess = (data: any) => ({type: GET_ALL_POSTS_REQUEST_SUCCESS, data});
export const getAllPostsRequestFailed = () => ({type: GET_ALL_POSTS_REQUEST_FAILED});
export const fetchGetAllPosts = () => ({type: FETCH_GET_ALL_POSTS});

export const getAllMyFriendsPostRequest = () => ({type: GET_ALL_MY_FRIENDS_POSTS_REQUEST});
export const getAllMyFriendsPostRequestSuccess = (data: any) => ({type: GET_ALL_MY_FRIENDS_POSTS_REQUEST_SUCCESS, data});
export const getAllMyFriendsPostRequestFailed = () => ({type: GET_ALL_MY_FRIENDS_POSTS_REQUEST_FAILED});
export const fetchGetAllMyFriendsPost = () => ({type: FETCH_GET_ALL_MY_FRIENDS_POSTS});

export const addPostRequest = () => ({type: REQUEST_ADD_POST});
export const addPostRequestSuccess = (data: any) => ({type: REQUEST_ADD_POST_SUCCESS, data});
export const addPostRequestFailed = () => ({type: REQUEST_ADD_POST_FAILED});
export const fetchAddPost = (data: any) => ({type: FETCH_ADD_POST, data});

export const deletePostRequest = () => ({type: REQUEST_DELETE_POST});
export const deletePostRequestSuccess = (data: any) => ({type: REQUEST_DELETE_POST_SUCCESS, data});
export const deletePostRequestFailed = () => ({type: REQUEST_DELETE_POST_FAILED});
export const fetchDeletePost = (data: any) => ({type: FETCH_DELETE_POST, data});
 
export const getAllUsersRequest = () => ({type: GET_ALL_USERS_REQUEST});
export const getAllUsersRequestSuccess = (data: any) => ({type: GET_ALL_USERS_REQUEST_SUCCESS, data});
export const getAllUsersRequestFailed = () => ({type: GET_ALL_USERS_REQUEST_FAILED});
export const fetchGetAllUsers = () => ({type: FETCH_GET_ALL_USERS});

export const addFriendRequestSuccess = (data: any, data1: any) => ({type: ADD_FRIEND_REQUEST_SUCCESS, data, data1})
export const fetchAddFriend = (data: any) => ({type: FETCH_ADD_FRIEND, data});
export const cancelRequestSuccess = (data: any, data1: any) => ({type: CANCEL_REQUEST_SUCCESS, data, data1})
export const fetchCancelRequest = (data: any) => ({type: FETCH_CANCEL_REQUEST, data})
export const applyInvitation = (data: any, data1: any, data2:any) => ({type: APPLY_INVITATION_SUCCESS, data, data1, data2})
export const fetchApplyInvitation = (data: any) => ({type: FETCH_APPLY_INVITATION, data})
export const rejectInvitationSuccess = (data: any, data1: any) => ({type: REJECT_INVITATION_SUCCESS, data, data1})
export const fetchRejectInvitation = (data: any) => ({type: FETCH_REJECT_INVITATION, data})

export const deleteFriendSuccess = (data:any, data1: any) => ({type: DELETE_FRIEND_SUCCESS, data, data1})
export const fetchDeleteFriend = (data: any) => ({type: FETCH_DELETE_FRIEND, data});

export const getAllMyInvitationsSuccess = (data: any) => ({type: GET_ALL_MY_INVITATIONS_SUCCESS, data})
export const fetchGetAllMyInvitation = () => ({type: FETCH_GET_ALL_MY_INVITATIONS})
export const getAllMyRequestsSuccess = (data: any) => ({type: GET_ALL_MY_REQUESTS_SUCCESS, data})
export const fetchgetAllMyRequests = () => ({type: FETCH_GET_ALL_MY_REQUESTS_SUCCESS})
export const getAllMyFriendsSuccess = (data: any) => ({type: GET_ALL_MY_FRIENDS_SUCCESS, data})
export const fetchgetAllMyFriends = () => ({type: FETCH_GET_ALL_MY_FRIENDS})

export const likePostSuccess = (data: any, data1: any, data2: any) => ({type: LIKE_POST_SUCCESS, data, data1, data2})
export const fetchLikePost = (data: any) => ({type: FETCH_LIKE_POST, data})

export const fetchCreateChat = (data: any) => ({type: FETCH_CREATE_CHAT, data})
export const createChatSuccsess = (data: any) => ({type: CREATE_CHAT_SUCCESS, data})

export const getChatSuccess = (data: any) => ({type: GET_CHAT_SUCCESS, data})
export const fetchGetChat = (data: any) => ({type: FETCH_GET_CHAT, data})





