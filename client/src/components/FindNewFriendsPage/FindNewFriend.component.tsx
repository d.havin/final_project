import { connect } from "react-redux";
import { useEffect, useState } from "react";
import { fetchGetAllUsers, fetchAddFriend, fetchDeleteFriend, fetchCancelRequest } from '../../actionCreator/user.action'
import './findNewFriend.css'

const FindNewFriend = (props: any) => {
    const [searchedUser, setSearchedUser] = useState('');
    const [userList, setUserList] = useState([])

    useEffect(() => {props.fetchGetAllUsers()}, [])
    useEffect(() => {setUserList(props.users)}, [props.users])  

    const handleRequest = (user: any) => {
        let candidate = {
            _id: user._id
        }
        props.fetchAddFriend(candidate)
    }
    const handleCancelRequest = (user: any) => {
        let candidate = {
            _id: user._id
        }
        props.fetchCancelRequest(candidate)
        console.log("cancel invitation")
    }
    const handleDeleteFriend = (user: any) => {
        let candidate = {
            _id: user._id
        }
        props.fetchDeleteFriend(candidate)
        console.log("delete friend")
    }
    return (
        <>
            <div className = "SearchBlock">
                Find user by name:
                <input className = "search-input" value = {searchedUser} onChange = {(e)=>setSearchedUser(e.target.value)} placeholder = "Write name"></input>
            </div>

            <div className = "UsersListBlock">
                <p>Users List:</p>
                {userList?.
                    filter((user:any) => (user.name?.toLowerCase() + '' + user.surname?.
                    toLowerCase()).includes(searchedUser.toLowerCase())).
                    map((user: any) => (
                        <div key = {user._id} className = "user_block">
                            <img className = "userAvatar" src = {user.avatar} alt = "img"></img>
                            <div className = "user-name">{user.name}</div>
                            <div className = "user-surname">{user.surname}</div>
                            {user._id == props.user._id? <span className = "its-you">Its you </span>:
                                user.friends.filter(
                                    (friend: any ) => (friend._id == props.user._id)
                                ).length > 0?
                                    <input className = "delete-friend" type = "submit" value = "Delete Friend" onClick={() => handleDeleteFriend(user)}/>:
                                    (user.invitations.filter(
                                        (invitation: any) => (invitation._id == props.user._id)
                                        ).length) > 0 ?
                                        <input className = "cancel-frienship-request" type = "submit" value = "Cancel request" onClick={() => handleCancelRequest(user)}/>:
                                        <input className = "send-frienship-request" type = "submit" value = "Send request" onClick={() => handleRequest(user)}/>
                            }
                        </div>
                ))}
            </div>
        </>
    )
}

const mapDispatchToProps =  (dispatch : any) => ({
    fetchGetAllUsers: () =>  {dispatch( fetchGetAllUsers())},
    fetchAddFriend: (data: any) => {dispatch(fetchAddFriend(data))},
    fetchDeleteFriend: (data:any) => {dispatch(fetchDeleteFriend(data))},
    fetchCancelRequest: (data:any) => {dispatch(fetchCancelRequest(data))}
});

const mapStateToProps = (state : any) => ({
    users : state.users.users,
    user: state.auth.logedUser
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(FindNewFriend);
