import './header.styles.css'

const Header = () => {
    return (
        <header className = "header-block">
            <img className = "header-img" src = "https://twibbon.blob.core.windows.net/twibbon/2015/140/22fab9a1-af0c-47fe-ae65-43fd76c6e3ab.gif" alt = "img"></img>
            <div className = "header-title" >Digital minions social network!</div>
        </header>
    )
}
export { Header }