import { connect } from "react-redux";
import { useEffect, useState } from "react";
import { io } from "socket.io-client";
import { fetchCreateChat, fetchGetChat } from '../../actionCreator/user.action'
import './ChatPage.css'

function Chat(props : any) { 
    const [message, setMessage] = useState("")

    const socket = io('http://localhost:3050', { transports: ['websocket', 'polling', 'flashsocket'] });
    const myId = props.user._id
    const chatId = window.location.pathname.substr(6);
    const data = {
        _id: chatId
    }

    useEffect(() => {props.fetchGetChat(data)}, [])
    useEffect(() => {
    socket.on('tweet', () => {props.fetchGetChat(data)
    })}, [])

    const sendMessage = async (message: any) => {socket.emit("sendMessage", { message, myId, chatId })}

    return (
        
        <div className = "chat-page">
            <div className= "chat-conteiner">
                <div className= "messages-conteiner">
                    {props.chat?.messages?.map((message: any) => (
                        
                        message.messageSendler._id === myId ? 
                            <div key = {message._id} className = "my-message">
                                <div className = "sendler-info">
                                    <img className = "my-avatar"src ={message.messageSendler.avatar} alt = "img"></img>
                                    <p className = "my-name"  >{message.messageSendler.name}</p>
                                </div>
                                <div className = "my-text">{message.text}</div> 
                            </div>
                        :
                            <div key = {message._id} className = "friend-message">
                                <p className = "friend-text">{message.text}</p> 
                                <div className = "reciver-info">
                                    <img className = "friend-avatar" src ={message.messageSendler.avatar} alt = "img"></img>
                                    <p className = "my-name"  >{message.messageSendler.name}</p>
                                </div>
                            </div>
                    ))}
                </div>  

                <div className = "message-menu">
                    <textarea className = "message-input" placeholder = "Write message..." value={message} onChange={(e) => setMessage(e.target.value)}/>
                </div>
                <input className = "message-submit" type = "submit" value = "Send" onClick={() => {sendMessage(message); setMessage("")}}/>
            </div>
        </div>  
    )
}

const mapDispatchToProps = (dispatch : any) => ({
    fetchCreateChat: (data:any) => {dispatch(fetchCreateChat(data))},
    fetchGetChat: (data: any) => {dispatch(fetchGetChat(data))}
});

const mapStateToProps = (state : any) => ({
    user : state.auth.logedUser,
    chat: state.chat.currentChat,
    friend: state.chat.friend
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Chat);
