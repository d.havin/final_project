import { connect } from "react-redux";
import { useHistory } from "react-router";
import { fetchCreateChat } from '../../actionCreator/user.action'
import './messagesPage.css'

const MessagesPage = (props: any) => {
    let history = useHistory();
    const redirect = (chat: any) =>{
        let _id = chat._id;
        history.push(`/chat/${_id}`);
    }

    return (
        <div>
            <h3>Your chats</h3>
            {props.user.chats?.map((chat: any) => (
                <div className = "person" key = {chat._id}>
                        <div className = "avatar-block">
                            <img className = "userAvatar" src ={chat.chatParticipants.filter((user:any)=>(user._id !== props.user._id))[0].avatar} alt = "img"></img>
                        </div>
                        <div className = "friend_short_info">{chat.chatParticipants.filter((user:any)=>(user._id !== props.user._id))[0].name} {chat.chatParticipants.filter((user:any)=>(user._id !== props.user._id))[0].surname}</div>  
                        <input type = "submit" className = "chat-button" onClick ={() => redirect(chat)} value = "redirect to chat"/>
                </div>  
            ))}
        </div>
    )
}

const mapDispatchToProps = (dispatch : any) => ({
    fetchCreateChat: (data:any) => {dispatch(fetchCreateChat(data))}
});

const mapStateToProps = (state : any) => ({
    user: state.auth.logedUser,
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(MessagesPage);
