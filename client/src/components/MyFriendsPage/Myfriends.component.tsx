import { connect } from "react-redux";
import { useEffect,  } from "react";
import { fetchGetAllMyInvitation, fetchgetAllMyRequests, fetchgetAllMyFriends, fetchDeleteFriend, 
    fetchCancelRequest, fetchApplyInvitation, fetchRejectInvitation, fetchCreateChat } from '../../actionCreator/user.action'
import './myfriends.styles.css'

const MyFriendsPage = (props: any) => {

    useEffect(() => {props.fetchgetAllMyFriends()}, [])
    useEffect(() => {props.fetchGetAllMyInvitation()}, [])
    useEffect(() => {props.fetchgetAllMyRequests()}, [])

    const handleApplyInvitation = (user: any) => {
        let candidate = {
            _id: user._id
        }
        props.fetchApplyInvitation(candidate)
    }
    const handleCancelRequest = (user: any) => {
        let candidate = {
            _id: user._id
        }
        props.fetchCancelRequest(candidate)
    }
    const handleDeleteFriend = (user: any) => {
        let candidate = {
            _id: user._id
        }
        props.fetchDeleteFriend(candidate)
    }
    const handleReject = (user: any) => {
        let candidate = {
            _id: user._id
        }
        props.fetchRejectInvitation(candidate)
    }
    const handleWrite = (user: any) => {
        let candidate = {
            canId: user._id,
            myId: props.user._id
        }
        props.fetchCreateChat(candidate)
    }

    return (
        <div className = "page-wrapper1">
            <div className = "friends-wrapper">
                <h3>Your friends list</h3>
                    {props.friends?.map((user : any) => (
                        <div className = "person" key = {user._id}>
                            <div className = "avatar-block">
                                <img className = "userAvatar" src ={user.avatar} alt = "img"/>
                            </div>
                            <div className = "friend_short_info">
                                {user.name} {user.surname}
                            </div>  
                            <input className = "write-message" type = "submit" value = "create chat" onClick={() => handleWrite(user)}/>
                            <input className = "delete-friend" type = "submit" value = "Delete" onClick={() => handleDeleteFriend(user)}/>
                        </div>  
                    ))} 
            </div> 
            <div className = "invitations-wrapper">
                <h3>Your invitations list</h3>
                {props.invitations != [] ? 
                    props.invitations?.map((user : any) => (
                    <div className = "person" key = {user._id}>
                        <div className = "avatar-block">
                            <img className = "userAvatar" src ={user.avatar} alt = "img"/>
                        </div>
                        <div className = "friend_short_info">{user.name} {user.surname}</div>
                        <input className = "delete-friend" type = "submit" value = "Reject" onClick={() => handleReject(user)}/>
                        <input className = "send-frienship-request" type = "submit" value = "Apply request" onClick={() => handleApplyInvitation(user)}/>
                    </div>  
                )): <span>No invitations... Be first!</span>} 
            </div>
            <div className = "requests-wrapper">
                <h3>Your requests list</h3>
                {props.requests != [] ? 
                    props.requests?.map((user : any) => (
                        <div className = "person" key = {user._id}>
                            <div className = "avatar-block">
                                <img className = "userAvatar" src ={user.avatar} alt = "img"/>
                            </div>
                            <div className = "friend_short_info">
                                {user.name} {user.surname}
                            </div>  
                            <input className = "cancel-frienship-request" type = "submit" value = "Cancel request" onClick={() => handleCancelRequest(user)}/>
                        </div>  
                    )) : <span>The larger group - the greater evil!!!</span>} 
            </div>

        </div>
    )
}

const mapDispatchToProps = (dispatch : any) => ({
    fetchGetAllMyInvitation: () =>  {dispatch( fetchGetAllMyInvitation())},
    fetchgetAllMyRequests: () =>  {dispatch( fetchgetAllMyRequests())},
    fetchgetAllMyFriends: () =>  {dispatch( fetchgetAllMyFriends())},

    fetchRejectInvitation: (data: any) => {dispatch(fetchRejectInvitation(data))},
    fetchDeleteFriend: (data:any) => {dispatch(fetchDeleteFriend(data))},
    fetchApplyInvitation: (data: any) => {dispatch(fetchApplyInvitation(data))},
    fetchCancelRequest: (data:any) => {dispatch(fetchCancelRequest(data))},

    fetchCreateChat: (data: any) => {dispatch(fetchCreateChat(data))},
});

const mapStateToProps = (state : any) => ({
    friends : state.users.myfriends,
    requests: state.users.requests,
    invitations: state.users.invitations,
    user: state.auth.logedUser,
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(MyFriendsPage);
