import { connect } from "react-redux";
import { useEffect } from "react";
import { fetchGetAllPosts, fetchLikePost } from '../../actionCreator/user.action';

const News = (props: any) => {

    useEffect(() => {props.fetchGetAllPosts()}, [])

    const handleLike = async (post: any) => {
        let likedPost = {
            _id: post._id
        }
        props.fetchLikePost(likedPost)
    }

    return (
        <div className = "allPostsBlock">
            AllNews:
            {props.posts.loading? <p className = "Loader">Loading...</p> : props.posts.error ? <p>Error.. </p> : 
                props.allposts?.map((post:any)=> (
                    <div className = "post-block" key = {post._id}>
                        <hr/>
                        <div className = "post_owner_info">
                            <img className = "Avatar_posts" src ={post.owner?.avatar} alt = "img"/>

                            <div>
                                <div>{post.owner?.name} {post.owner?.surname}</div>
                                <div>{post.date}</div>
                            </div>
                        </div>

                        <div className = "Post_body">
                            {post.description? <p className = "Post_body_description">{post.description}</p>: null}
                            {post.media ? <img className = "Post_body_img" src ={post.media} alt = "media"/>: null}
                        </div>

                        <div className = "post_options">
                            <input className = "like_button" type = "submit" value = "like" onClick ={() =>handleLike(post)}/>
                            <span className ="like-value">{post.likes.length}</span>
                            <input className = "comment_button" type = "submit" value = "comments"/>
                        </div>
                    </div>
                ))
            }
            
        </div>
    )
}

const mapDispatchToProps = (dispatch : any) => ({
    fetchGetAllPosts: () => {dispatch(fetchGetAllPosts())},
    fetchLikePost: (data: any) => {dispatch(fetchLikePost(data))}
});

const mapStateToProps = (state : any) => ({
    allposts : state.posts.allposts,
    posts: state.posts
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(News);

