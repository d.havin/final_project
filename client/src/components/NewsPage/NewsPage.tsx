import { useState } from 'react'
import { NewsNavBar } from './NewsNavBar'
import  MyfriendsPosts from './MyFriendsPosts.component'
import News from './News.component'
import MyPosts from '../ProfilePage/MyPosts.component'

const NewsPage = () => {
    const [component, setComponent] = useState('news')

    let renderSwitch = function(curr: any){
        switch (curr) {
        case "news": return <News/>
        case "myfriendsposts": return <MyfriendsPosts/>
        case "myposts": return <MyPosts/>
        }
    }

    return (
        <div className = "news-page-wrapper">
            <NewsNavBar handleFunc = {setComponent}/>
            {renderSwitch(component)}
        </div>
    )
}
    
export default NewsPage