import { connect } from "react-redux";
import { fetchDeletePost, fetchGetAllMyPosts, fetchLikePost } from '../../actionCreator/user.action'
import './myPosts.css'
import { useEffect } from "react";

const MyPosts = (props: any) => {

    useEffect(() => {props.fetchGetAllMyPosts()}, [])

    const handleDeletePost = (post:any) => {
        let deletedPost = {
            _id: post._id
        }
        props.fetchDeletePost(deletedPost);
    }

    const handleLike = (post: any) => {
        let likedPost = {
            _id: post._id
        }
        props.fetchLikePost(likedPost)  
    }

    return (
        <div className = "allMyPostsBlock">
            <p>Your publications:</p>
            {props.myposts?.map((post: any) => (
                <div key = {post._id} className = "Post_block">
                    <hr/>
                    <div className = "post_owner_info">
                        <img className = "Avatar_posts" src ={props.user.avatar} alt = "img"/>
                        <div>
                            <div>{props.user.name} {props.user.surname}</div>
                            <div>{post.date}</div>
                        </div>
                    </div>

                    <div className = "Post_body">
                        {post.description? <p className = "Post_body_description">{post.description}</p>: null}
                        {post.media ? <img className = "Post_body_img" src ={post.media} alt = "media"/>: null}
                    </div>

                    <div className = "post_options">
                        <input className = "like_button" type = "submit" value = "like" onClick ={() =>handleLike(post)}/>
                        <span className ="like-value">{post.likes.length}</span>
                        <input className = "comment_button" type = "submit" value = "comments"/>
                        <input className = "Button_delete_post" type = "submit" onClick={() => handleDeletePost(post)} value = "Delete post"/>
                    </div>
                </div>    
            )).reverse()}
        </div>
    )
}

const mapStateToProps = (state: any) => ({
    user: state.auth.logedUser,
    myposts: state.posts.myposts
});

const mapDispatchToProps = (dispatch: any) => ({
    fetchGetAllMyPosts: () => {dispatch(fetchGetAllMyPosts())},
    fetchDeletePost: (data: any) => {dispatch(fetchDeletePost(data))},
    fetchLikePost: (data: any) => {dispatch(fetchLikePost(data))}
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(MyPosts);