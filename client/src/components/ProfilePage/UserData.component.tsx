import { useState, useEffect } from 'react';
import { connect } from "react-redux";
import { fetchChangeAvatarRequest } from '../../actionCreator/user.action'
import './userData.styles.css'

const UserData = (props: any) => {
    const [ editedAvatar, setAvatar ] = useState('')
    let [ avatarInputVisibility, setAvatarInputVisibility ] = useState(false)
    let [ valueAva, setValueAva ] = useState('Change avatar')
    let [ avaImg, setAvaImg] = useState(props.user.avatar)
    // const [editedName, setName] = useState('');
    // const [editedSurName, setSurName] = useState('');
    // const [editedEmail, setEmail] = useState('');
    // const [editedPassword, setPassword] = useState('');

    const handleChangeVision = () => {
        avatarInputVisibility === false ? setAvatarInputVisibility(avatarInputVisibility = true) : setAvatarInputVisibility(avatarInputVisibility = false)
        valueAva === 'Change avatar' ? setValueAva(valueAva = "cancel") : setValueAva(valueAva = 'Change avatar')
    }

    useEffect(() => {setAvaImg(props.user.avatar)}, [props.user.avatar])

    const handleChangeAvatar = () => {
        let avatar = {
            avatar: editedAvatar,
        };
        props.fetchChangeAvatarRequest(avatar)
        setAvaImg(props.user.avatar)
        setAvatarInputVisibility(avatarInputVisibility = false)
    }

    
    return (
        <>
            <div className = "userDataBlock">
                <div className = "avatarBlock">
                    <img className="Avatar" src = {avaImg} alt = "img"></img>
                </div>
                <div className = "descriptionBlock">
                    <h1>Profile</h1>
                    <p className = "userDataOption">Name: {props.user.name}</p>
                    <p className = "userDataOption">Surname: {props.user.surname}</p>
                    <p className = "userDataOption">Email: {props.user.email}</p>
                </div>
            </div>
            <div className = "changeAvaBlock">
                <div>
                    <input className = "change_ava_button" type = "submit" onClick = {handleChangeVision} value = {valueAva}/>
                </div>
                {avatarInputVisibility === false ? <p className = "ava_message">Upload a picture and make your minion unique!!!</p> :
                <div>
                    <input className="change_ava_input" placeholder = "Write Url" value={editedAvatar} onChange={(e) => setAvatar(e.target.value)}/> 
                    <input className="change_ava_button" style = {{backgroundColor: "rgba(124, 226, 124, 0.84)", color: "white"}} type = "submit" onClick = {handleChangeAvatar} value = "Save"/>
                </div>
                }
            </div>
        </>
    )
}

const mapStateToProps = (state: any) => ({
    user: state.auth.logedUser
});

const mapDispatchToProps = (dispatch: any) => ({
    fetchChangeAvatarRequest: (data : any) => {dispatch(fetchChangeAvatarRequest(data))}
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(UserData);