import UserData  from './UserData.component'
import AddPost  from './AddPost.component'
import MyPosts  from './MyPosts.component'

function NewProfile(props:any) {
  return (
    <div>
        <UserData/>
        <AddPost/>
        <MyPosts/>
    </div>
  );
}

export default NewProfile;
