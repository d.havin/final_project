import React, { useState } from 'react';
import { connect } from "react-redux";
import { Link } from 'react-router-dom';
import { fetchRegistrationNewUser } from '../../actionCreator/user.action'

const RegistrationForm = (props: any) => {
    const [inputedName, setName] = useState("");
    const [inputedSurName, setSurName] = useState("");
    const [inputedEmail, setEmail] = useState("");
    const [inputedPassword, setPassword] = useState("");

    const handleReg = () => {
        let user = {
            name: inputedName,
            surname: inputedSurName,
            email: inputedEmail,
            password: inputedPassword
        };
        props.fetchRegistrationNewUser(user);
        setEmail('');
        setPassword('');
        setName('');
        setSurName('');
    }
 
    return (
        <div className = "RegistrationBlock">
            <div className = "Title_Registration">Registration</div>
            <input className = "Input_Registration" placeholder ="Name" value={inputedName} onChange={(e) => setName(e.target.value)}/>
            <input className = "Input_Registration" placeholder ="Surname" value={inputedSurName} onChange={(e) => setSurName(e.target.value)}/>
            <input className = "Input_Registration" placeholder ="Email" value={inputedEmail} onChange={(e) => setEmail(e.target.value)}/>
            <input className = "Input_Registration" placeholder = "Password" value={inputedPassword} onChange={(e) => setPassword(e.target.value)}/>
            <input className="Button_Registration" type = "submit" onClick={handleReg} value = "Register now"/>
            <div className = "already">
                Already have an account?<Link to = "/"><p className = "link-reg">Sign in</p></Link>
            </div>
            <div className = "Status_block" >
                {props.auth.loading? <p>Loading...</p> : props.auth.error ? <p>Error... Try again</p> : props.auth.message}
            </div>
        </div>
    )
}
const mapStateToProps = (state : any) => ({
    auth : state.auth
});

const mapDispatchToProps = (dispatch : any) => ({
    fetchRegistrationNewUser: (data : any) => {console.log(data); dispatch(fetchRegistrationNewUser(data))}
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(RegistrationForm);
