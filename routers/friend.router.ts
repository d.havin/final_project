import express from 'express';
import { getAllInvitations, getAllRequests, getAllFriends, removeFriend } from '../controllers/friends.controller'

const friendsRouter: any = express.Router();

friendsRouter.get('/invitations', getAllInvitations)
friendsRouter.get('/requests', getAllRequests)
friendsRouter.get('/myfriends', getAllFriends)
friendsRouter.delete('/deleteFriend', removeFriend)

export { friendsRouter }