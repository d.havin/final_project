import express from 'express';
import {  createChat, findMyChat } from '../controllers/chat.controller';

const chatRouter: any = express.Router();

chatRouter.post('/createchat', createChat)
chatRouter.post('/getchat', findMyChat)

export { chatRouter }