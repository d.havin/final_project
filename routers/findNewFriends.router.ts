import express from 'express';
import { getAllUsers, createFriendshipRequest, cancelFriendshipRequest, applyInvitation, rejectInvitation } from '../controllers/friends.controller'

const findNewFriendsRouter: any = express.Router();

findNewFriendsRouter.get('/allUsers', getAllUsers)

findNewFriendsRouter.put('/addFriend', createFriendshipRequest)
findNewFriendsRouter.put('/cancelRequest', cancelFriendshipRequest)
findNewFriendsRouter.post('/apply', applyInvitation)
findNewFriendsRouter.put('/rejectInvitation', rejectInvitation)

export { findNewFriendsRouter }
