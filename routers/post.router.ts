import express from 'express';
import { createPost, deletePost, getAllPosts, getMyFriendsPost, getMyPosts, likePost } from '../controllers/post.controller';

const postRouter: any = express.Router();

postRouter.post('/create', createPost)
postRouter.get('/myposts', getMyPosts)
postRouter.get('/myfriendsposts', getMyFriendsPost)
postRouter.delete('/delete', deletePost)
postRouter.get('/allposts', getAllPosts)
postRouter.put('/like', likePost)

export { postRouter }