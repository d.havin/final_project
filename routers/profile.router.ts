import express from 'express';
const profileRouter = express.Router();
import { changeAvatar } from '../controllers/profile.controller'
import { authMiddleware  } from '../controllers/auth.controller'

profileRouter.post('/changeavatar', authMiddleware, changeAvatar) //authMiddleware ,

export { profileRouter }