import mongoose from "mongoose";
import autopopulate from "mongoose-autopopulate";
import { IUser } from "./user.model";
import { IPost } from "./post.model";
const { Schema } = mongoose;

interface IComment extends mongoose.Document {
  commentedPost: IPost;
  commentOwner: IUser;
  commentOwnerAva: String;  // ???????
  commentText: String;
  date: Date;
}

const commentSchema = new Schema(
  {
    commentedPost: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Post",
      require: true,
    },
    commentOwner: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
      require: true,
    },
    commentOwnerAva: { type: String },
    commentText: { type: String },
    date: { type: Date, default: Date.now() },
  },
  { versionKey: false }
);

commentSchema.plugin(autopopulate);
const Comment = mongoose.model<IComment>("Comment", commentSchema);

export { Comment, IComment, commentSchema };
