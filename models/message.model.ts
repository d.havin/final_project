import mongoose from "mongoose";
import autopopulate from "mongoose-autopopulate";
import { IUser } from "./user.model";
import { IChat } from "./chat.model";
const { Schema } = mongoose;

interface IMessage extends mongoose.Document {
    chat: mongoose.Schema.Types.ObjectId;
    messageSendler: IUser;
    text: String;
    date: Date;
}

const messageSchema = new Schema(
  {
    chat: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Chat", 
    },
    messageSendler: {
      type: mongoose.Schema.Types.ObjectId,     
      ref: "User",
      autopopulate: { maxDepth: 2 },
      require: true,
    },
    text: {type: String, require: true},
    date: { type: Date, default: Date.now() },
  },
  { versionKey: false }
);

messageSchema.plugin(autopopulate);
const Message = mongoose.model<IMessage>("Message", messageSchema);

export { Message, IMessage, messageSchema };
